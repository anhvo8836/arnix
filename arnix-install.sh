#!/bin/bash

clear

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

######################
# ENVIRONMENT CHECKS #
######################
# In this section, we will perform various tests to see if the current system is ready for installation.
# UEFI Check
firmware_test

# Test Internet Connection
net_test

# Root Privilege Check
sudo_check

sleep 1;clear

#########
# START #
#########
# Start Screen
sh /root/arnix/arnix-start-screen.sh

###########################
# DISK PREPARTION - QUERY #
###########################
# Query
# Execute a script to be guided through the disk query portion.
# See disk-query.sh for further details. 
source /root/arnix/disks/disk-query.sh

###################################
# INSTALLATION PREPARTION - QUERY #
###################################
# Query
# Execute a script to be guided through the pacstrap query portion.
# See pacstrap-query.sh for further details.
source /root/arnix/pacstrap/pacstrap-query.sh

###########################
# DISK PREPARTION - SETUP #
###########################
# Execute the appropriate script to create partitions, format, and mount them, according to your filesystem choice.
case $FILESYSTEM in

    1 ) # EXT4

        source /root/arnix/disks/ext4.sh
       
    ;;

    2 ) # BTRFS

        source /root/arnix/disks/btrfs.sh

    ;;

    * ) # Default option

        source /root/arnix/disks/ext4.sh


    ;;

esac

############################
# DISK PREPARATION RESULTS #
############################
# Show the results of the disk preparation stage
echo
fg_blue ; print_centered "-" "-"; reset_colour 
echo
lsblk -f
echo

###################################
# INSTALLATION PREPARTION - SETUP #
###################################
# Execute a script to set up the installation environment.
# See pacstrap-setup.sh for further details.
sh /root/arnix/pacstrap/pacstrap-setup.sh

################
# INSTALLATION #
################
# Execute a script to install the system.
# See pacstrap.sh for further details.
sh /root/arnix/pacstrap/pacstrap.sh
echo

################################
# CHROOT ENVIRONMENT VARIABLES #
################################
# We will pass all the environment variables exported by the various scripts into the chroot environment.
export DEVICE_TYPE
export DEVICE
export ESP_SIZE
export ROOT_SIZE
export ESP
export ROOT
export HOME
export ROOT_UUID

export KERNEL
export KERNEL_HEADERS
export TEXT_EDITOR
export VIDEO
export NVIDIA
export AUDIO
export DESKTOP
export DESKTOP_EXTRAS
export PRINTERS
export BROWSER
export DEVELOPMENT_TOOLKIT
export VIRTUALIZATION_TOOLKIT

export CPU
export FILESYSTEM
export MICROCODE
export INITRAMFS

##########
# CHROOT #
##########
# Execute a function to set up the chroot environment.
# See chroot_setup in functions.sh for further details.
chroot_setup
arch-chroot /mnt sh arnix/arnix-chroot.sh

#######
# END #
#######
# End Screen
sh /root/arnix/arnix-end-screen.sh
