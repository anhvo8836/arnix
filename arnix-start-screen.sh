#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

################
# START SCREEN #
################
# You will be greated with a welcome message and a prompt to start the installer.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "ARNIX INSTALLER"
fg_blue ; print_centered "=" "=" ; reset_colour
echo
print_centered "Welcome to the Arnix Installer"
print_centered "This installer is designed to guide you through the process of installing Arch Linux"
print_centered "NOTE : This is not an Arch Linux distribution. This is will install mainline Arch Linux."
echo
create_install_log
read -r | print_centered "Press [ENTER] to begin"
