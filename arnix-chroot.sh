#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "SYSTEM CONFIGURATION"
fg_blue ; print_centered "=" "=" ; reset_colour
echo

########################
# SYSTEM OPTIMIZATIONS #
########################
# Execute a script that will run various tasks to optimize the system.
# See systemm-optimization.sh for further deatils.
sh /arnix/chroot/system-optimizations.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#############################
# TIMEZONE AND LOCALIZATION #
#############################
# Execute a script that will set the system timezone and system language.
# See localization.sh for further details.
sh /arnix/chroot/localization.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#########################
# NETWORK CONFIGURATION #
#########################
# Execute a script that configure the network connection on the new system.
# See network-configuration.sh for further details.
sh /arnix/chroot/network-configuration.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#########################
# ENVIRONMENT VARIABLES #
#########################
# Execute a script that will set the system's environment variables. 
# See environment-variables.sh for further details.
sh /arnix/chroot/environment-variables.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

####################
# SYSTEMD SERVICES #
####################
# Execute a script that will autostart various system-d services.
# See system-services.sh for further details.
sh /arnix/chroot/system-services.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##############
# BOOTLOADER #
##############
# Execute a script to install and configure your chosen bootloader.
# See refind-configuration.sh or grub-configuration.sh for further details.
case $BOOTLOADER in

    1 )

        sh /arnix/chroot/grub-configuration.sh

    ;;

    2 )

        sh /arnix/chroot/systemd-boot-configuration.sh

    ;;

    3 )

        sh /arnix/chroot/refind-configuration.sh

    ;;

    * )

        sh /arnix/chroot/grub-configuration.sh

    ;;
esac
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#################
# USER ACCOUNTS #
#################
# Execute a script that will guide you through setting up your user account and root account.
# See user-accounts.sh for further details.
sh /arnix/chroot/user-accounts.sh
echo

