#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we can create new variables or source our variables file to use its variables.
source /root/arnix/variables.sh

TERM_ROWS="$(tput lines)"
TERM_COLS="$(tput cols)"
PING=$(ping -c 1 archlinux.org | wc -l)

export TERM_ROWS
export TERM_COLS
export PING

#############
# FUNCTIONS #
#############
# We will use this file to create univeral functions that can be sourced.
# Centre Text
# This function can be used to centre text within the script
# Usage : print_centered "text to be centered" "filler character"
function print_centered {
     [[ $# == 0 ]] && return 1

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#1}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$1";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len) / 2 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "%s%s%s" "$filler" "$1" "$filler"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "%s" "${ch}"
     printf "\n"

     return 0
}

# Root Privlege Check
# This function will confirm that the installer has been executed with root (sudo) privleges
# Usage : sudo_test
function sudo_check {
    if [[ "$EUID" -ne 0 ]]; then

        echo -e "Root privilege... [ ${FG_RED}FAILED${END} ]"
        echo
        print_centered "Please restart the installer with sudo privileges. The installer will now exit"
        sleep 2;clear;exit

    else

        echo -e "Root privilege check... [ ${FG_GREEN}PASSED${END} ]"

    fi
}

# Firmware Test
# This function will check to see what firmware the system is currently using.
# If the system is not booted in UEFI mode, the function will display an error message and the installer will exit.
# Usage : firmware_test
function firmware_test {
    if [ ! -d "/sys/firmware/efi/efivars" ]; then

        echo -e "[ ${FG_RED}FAILED${END} ] The system is not booted in UEFI mode"
        echo
        echo "Please reboot your system in UEFI mode and try again. The installer will now exit."
        sleep 2;clear;exit

    else

        echo -e "Firmware check... [ ${FG_GREEN}PASSED${END} ]"

    fi
}

# Connection Test
# This function will test the current system's internet connection to see if it's active.
# Usage : net_test
function net_test {
    if [ "$PING" -eq 0 ]; then

        echo -e "[ ${FG_RED}FAILED${END} ] The system is not connected to the internet"
        echo
        print_centered "Please connect to the internet and try again. The installer will now exit."
        sleep 2;clear;exit

    else

        echo -e "Internet connection test... [ ${FG_GREEN}PASSED${END} ]"

    fi
}

# Create Logs 
# This function will create a folder and temporarily blank files in the Live ISO's /var/log directory. 
# These will contain pacstrap install logs. This will allow you to review the output instead of 
# re-routing into the void (ie. /dev/null).
# This will be copied to the new installation in the same location for review.
function create_install_log {

    mkdir -p /var/log/arnix
    touch /var/log/arnix/arnix.log && echo -e "ARNIX INSTALL LOG\n" >> /var/log/arnix/arnix.log 
 
}

# Refresh Mirrors
# This function can be used to refresh your mirrorlist
# Usage refresh_mirrors
function refresh_mirrors {

    echo "Please wait while a new mirrorlist is being generated"
    echo ""
    echo "Mirrors : Canada, US"
    echo "# of Mirrors : 5"
    echo "Protocol : http, https"
    echo "Sort by : Speed"
    echo "Save Location : /etc/pacman.d/mirrorlist"
    reflector --country 'Canada,US' --score 50 --fastest 5 --protocol http,https --sort rate --save /etc/pacman.d/mirrorlist &> /var/log/arnix/arnix.log
    echo
    echo "A new list of mirrors has been generated"

}

# Pacman Mods
# This function will modify the pacman.conf file and enable the following;
# Colorized pacman output
# Parallel Downloads (will be increased to 25)
# Your personal repository
# Usage pacman_mods
function pacman_mods {

    # Color Option
    # Enable the color option in pacman.conf.
    # This will colorize the pacstrap output.
    sed -i 's/#Color/Color/' /etc/pacman.conf

    # Parallel Downloads
    # Enable parallel downloads and increase the amount to 25.
    # This will help speed up the installation.
    echo "Parallel downloads will be enabled and increased to 25"
    sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 25/' /etc/pacman.conf
    echo

    # Multilib Repository
    # Enable the Multilib repository.
    # This will give you access to any 32-bit libraries required.
    echo "The multilib repository will be emabled"
    sed -i '90,91s/^#//' /etc/pacman.conf
    echo

    # Personal Repository
    # Add your personal repository to the pacman.conf
    # This will allow you to install custom built or AUR packages during the installation process.
    echo "Your personal repository will be added"
    echo -e "\n[personal]\nSigLevel = Optional TrustAll\nServer = https://gitlab.com/anhvo8836/packages/-/raw/main" >> /etc/pacman.conf

}

# Refresh
# This function will refresh the Pacman repository databases and obtain a new Arch Linux keyring.
# This should help prevent errors during installation.
function refresh {

echo "The repository databases and Arch Linux keyring will now be refreshed"
pacman -Syyy archlinux-keyring --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
echo

}

# Error check
# This function will check the exit status of the last command run.
# If the exit status is equal to 1 (ie. an err occurred, the function will dis play an error message
# and trigger an exit command.
# Otherwise, a success message will be displayed
# Usage : error_check after a command (ie. sudo pacman -S linux; error_check)
function error_check {

    if [[ $? -eq 1 ]]; then

        echo
        echo -e "[ ${FG_L_RED}ERROR${END} ] An ${FG_L_RED}error${END} has occurred..."

    else

        echo
        echo -e "[ ${FG_L_GREEN}DONE${END} ] All packages have been installed ${FG_L_GREEN}successfully!${END}"

    fi

}

# Chroot Setup
# This function will perform various tasks to set up the chroot environment.
# Usage : chroot_setup
function chroot_setup {

    # Generate FSTab file
    genfstab -U /mnt > /mnt/etc/fstab
    
    # Copy the Live ISO's mirrorlist to the new installation
    cat /etc/pacman.d/mirrorlist > /mnt/etc/pacman.d/mirrorlist

    # Copy the live ISO's pacman.conf to the new installation
    cat /etc/pacman.conf > /mnt/etc/pacman.conf

    # Copy the Arnix folder to the new installation
    cp -r /root/arnix /mnt

    # Edit functions file to point to where the variables file will be on the new system
    sed -i 's/source \/root\/arnix\/variables.sh/source \/arnix\/variables.sh/' /mnt/arnix/functions.sh

    # Move Install Log
    # Move the install log from the live ISO to the new root partition in the same location
    cp -r /var/log/arnix /mnt/var/log 

}

# Clean Up
# This function will perform 3 clean-up tasks.
# This is meant to be ran prior to reboot the system.
# Usage : clean_up
function clean_up {

    # Remove Installation Scripts
    rm -rf /mnt/arnix

    # Edit Pacman.conf
    # Remove personal repository mirror from the system's pacm.conf file
    sed -i '99,101d' /mnt/etc/pacman.conf

    # Unmount Partitions
    # Unmount all partitions currently mounted to /mnt
    umount -o &> /dev/null

}

# These functions will set the terminal's foreground colour.
# Each colour is in the function name.
# Usage : fg_(Options : black, red, green, yellow, blue, magenta, or cyan)
function fg_black {

    tput setaf 0

}

function fg_red {

    tput setaf 1

}

function fg_green {

    tput setaf 2

}

function fg_yellow {

    tput setaf 3

}

function fg_blue {

    tput setaf 4

}

function fg_magenta {

    tput setaf 5

}

function fg_cyan {

    tput setaf 6

}

# These functions will set the terminal's foreground colour.
# Each colour is in the function name.
# Usage : bg_(Options : black, red, green, yellow, blue, magenta, or cyan)
function bg_black {

    tput setbf 0

}

function bg_red {

    tput setbf 1

}

function bg_green {

    tput setbf 2

}

function bg_yellow {

    tput setbf 3

}

function bg_blue {

    tput setbf 4

}

function bg_magenta {

    tput setbf 5

}

function bg_cyan {

    tput setbf 6

}

# This function will reset the terminal's colours back to default.
# Usage : reset_colour
function reset_colour {

    tput sgr0

}

# Export functions
export -f print_centered
export -f sudo_check
export -f firmware_test
export -f net_test
export -f create_install_log
export -f refresh_mirrors
export -f pacman_mods
export -f refresh
export -f error_check
export -f chroot_setup
export -f clean_up
export -f fg_black
export -f fg_red
export -f fg_green
export -f fg_yellow
export -f fg_blue
export -f fg_magenta
export -f fg_cyan
export -f bg_black
export -f bg_red
export -f bg_green
export -f bg_yellow
export -f bg_blue
export -f bg_magenta
export -f bg_cyan
export -f fg_black
export -f reset_colour

