#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# We can use this file to declare any variables we may need within the installer.
# Be sure to export any variables declared here so they can be accessed from other scripts launched using this script.

# Text
# Formatting
NORMAL="\e[0m"
BOLD="\e[1m"
DIM="\e[2m"
ITALIC="\e[3m"
UNDERLINE="\e[4m"
BLINKING="\e[5m"
REVERSE="\e[6m"
INVISIBLE="\e[7m"

export NORMAL
export BOLD
export DIM
export ITALIC
export UNDERLINE
export BLINKING
export REVERSE
export INVISIBLE

# Reset Text Modifier(s)
END="\e[0m"

export END

# Foreground colour
FG_DEFAULT="\e[39m"
FG_BLACK="\e[30m"
FG_RED="\e[31m"
FG_GREEN="\e[32m"
FG_ORANGE="\e[33m"
FG_BLUE="\e[34m"
FG_MAGENTA="\e[35m"
FG_CYAN="\e[36m"
FG_L_GRAY="\e[37m"
FG_D_GRAY="\e[1;30m"
FG_L_RED="\e[1;31m"
FG_L_GREEN="\e[1;32m"
FG_YELLOW="\e[1;33m"
FG_L_BLUE="\e[1;34m"
FG_L_PURPLE="\e[1;35m"
FG_L_CYAN="\e[1;36m"
FG_WHITE="\e[1;37m"

export FG_DEFAULT
export FG_BLACK
export FG_RED
export FG_GREEN
export FG_ORANGE
export FG_YELLOW
export FG_BLUE
export FG_MAGENTA
export FG_CYAN
export FG_L_GRAY
export FG_D_GRAY
export FG_L_RED
export FG_L_GREEN
export FG_L_BLUE
export FG_L_PURPLE
export FG_L_CYAN
export FG_WHITE

# Background colour
BG_DEFAULT="\e[49m"
BG_BLACK="\e[40m"
BG_RED="\e[41m"
BG_GREEN="\e[42m"
BG_ORANGE="\e[43m"
BG_BLUE="\e[44m"
BG_MAGENTA="\e[45m"
BG_CYAN="\e[46m"
BG_L_GRAY="\e[47m"
BG_D_GRAY="\e[1;100m"
BG_L_RED="\e[1;101m"
BG_L_GREEN="\e[1;102m"
BG_YELLOW="\e[1;103m"
BG_L_BLUE="\e[1;104m"
BG_L_PURPLE="\e[1;105m"
BG_L_CYAN="\e[1;106m"
BG_WHITE="\e[1;107m"

export BG_DEFAULT
export BG_BLACK
export BG_RED
export BG_GREEN
export BG_ORANGE
export BG_YELLOW
export BG_BLUE
export BG_MAGENTA
export BG_CYAN
export BG_L_GRAY
export BG_D_GRAY
export BG_L_RED
export BG_L_GREEN
export BG_L_BLUE
export BG_L_PURPLE
export BG_L_CYAN
export BG_WHITE

# User Prompt Arrow
ARROW="${FG_L_GREEN}>${END} "

export ARROW

# Packages Header
PACKAGES_HEADER="${FG_YELLOW}[[${END} ${FG_L_CYAN}Packages${END} ${FG_YELLOW}]]${END}"

export PACKAGES_HEADER
