#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

##############################
# SYSTEM CONFIGURATION FILES #
##############################
# Copy over system configuration files and pacman hooks

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "SYSTEM CONFIGURATION FILES" 
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Timeshift
echo "The main Timeshift-Autosnap config file will be updated"
sudo sed -i 's/maxSnapshots=3/maxSnapshots=1/' /etc/timeshift-autosnap.conf

# Pacman Hooks
echo "Your pacman hooks will now be copied to the new system"
sudo -s mkdir -p /etc/pacman.d/hooks
sudo -s cp -r /home/"$USER"/Git/Arch/hooks /etc/pacman.d

