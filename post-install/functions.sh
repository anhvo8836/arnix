#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we can create new variables or source our variables file to use its variables.
source /home/"$USER"/post-install/variables.sh


TERM_ROWS="$(tput lines)"
TERM_COLS="$(tput cols)"
PING=$(ping -c 1 archlinux.org | wc -l)

export TERM_ROWS
export TERM_COLS
export PING

#############
# FUNCTIONS #
#############
# We will use this file to create univeral functions that can be sourced.
# Centre Text
# This function can be used to centre text within the script
# Usage : print_centered "text to be centered" "filler character"
function print_centered {
     [[ $# == 0 ]] && return 1

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#1}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$1";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len) / 2 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "%s%s%s" "$filler" "$1" "$filler"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "%s" "${ch}"
     printf "\n"

     return 0
}

# Root Privlege Check
# This function will confirm that the installer has been executed with root (sudo) privleges
# Usage : sudo_test
function sudo_check {
    if [[ "$EUID" -ne 0 ]]; then

        echo -e "Root privilege... [ ${FG_RED}FAILED${END} ]"
        echo
        print_centered "Please restart the installer with sudo privileges. The installer will now exit"
        sleep 2;clear;exit

    else

        echo -e "Root privilege check... [ ${FG_GREEN}PASSED${END} ]"

    fi
}


# Error check
# This function will check the exit status of the last command run.
# If the exit status is equal to 1 (ie. an err occurred, the function will dis play an error message
# and trigger an exit command.
# Otherwise, a success message will be displayed
# Usage : error_check after a command (ie. sudo pacman -S linux; error_check)
function error_check {

    if [[ $? -eq 1 ]]; then

        echo
        echo -e "[ ${FG_L_RED}ERROR${END} ] An ${FG_L_RED}error${END} has occurred..."

    else

        echo
        echo -e "[ ${FG_L_GREEN}DONE${END} ] Completed ${FG_L_GREEN}successfully!${END}"

    fi

}

# These functions will set the terminal's foreground colour.
# Each colour is in the function name.
# Usage : fg_(Options : black, red, green, yellow, blue, magenta, or cyan)
function fg_black {

    tput setaf 0

}

function fg_red {

    tput setaf 1

}

function fg_green {

    tput setaf 2

}

function fg_yellow {

    tput setaf 3

}

function fg_blue {

    tput setaf 4

}

function fg_magenta {

    tput setaf 5

}

function fg_cyan {

    tput setaf 6

}

# These functions will set the terminal's foreground colour.
# Each colour is in the function name.
# Usage : bg_(Options : black, red, green, yellow, blue, magenta, or cyan)
function bg_black {

    tput setbf 0

}

function bg_red {

    tput setbf 1

}

function bg_green {

    tput setbf 2

}

function bg_yellow {

    tput setbf 3

}

function bg_blue {

    tput setbf 4

}

function bg_magenta {

    tput setbf 5

}

function bg_cyan {

    tput setbf 6

}

# This function will reset the terminal's colours back to default.
# Usage : reset_colour
function reset_colour {

    tput sgr0

}

# Export functions
export -f print_centered
export -f sudo_check
export -f error_check
export -f fg_black
export -f fg_red
export -f fg_green
export -f fg_yellow
export -f fg_blue
export -f fg_magenta
export -f fg_cyan
export -f bg_black
export -f bg_red
export -f bg_green
export -f bg_yellow
export -f bg_blue
export -f bg_magenta
export -f bg_cyan
export -f fg_black
export -f reset_colour

