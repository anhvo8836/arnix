#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

####################
# GIT REPOSITORIES #
####################
# Clone git repositories

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "==[ GIT REPOSITORIES ]=="
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Test to see if Git and Git-LFS are installed on the system. If not, install them.
if ! command -v git &> /dev/null

then

    echo -e "${FG_L_RED}Git is not installed on the system${END}"
    echo "It will now be installed"
    echo
    sudo pacman -S git --needed --noconfirm --disable-download-timeout &> /dev/null
    echo -e "${FG_L_GREEN}Git has been installed"

fi

if ! command -v git-lfs &> /dev/null

then

    echo -e "${FG_L_RED}Git-LFS is not installed on the system${END}"
    echo "It will now be installed"
    echo
    sudo pacman -S git-lfs --needed --noconfirm --disable-download-timeout &> /dev/null
    echo -e "${FG_L_GREEN}Git-LFS has been installed"

fi

# Git Credentials
# Set the system's global git credentials
echo "The system's git credentials will be set"
echo "User Name : Anh Vo"
echo "Email : anhthevo.1989@gmail.com"
git config --global user.name "Anh Vo"
git config --global user.email "anhthevo.1989@gmail.com"
echo

# Initialise Git-LFS
# Initialise Git-LFS to be able to push from any repositories that contain Git-LFS items
echo "Git-LFS will be installed and initialised"
git lfs install
echo

# Set Http.Postbuffer to 1GB
# Set the system's git https.postbuffer to 1GB to prevent any push eerrors when pushing large updates
echo "The system's git https.postbuffer will be set to 1GB"
echo "This will help prevent errors when pushing large updates to your remote repositories"
git config --global http.postBuffer 1048576000
echo
print_centered "-" "-"
echo

# Create Git directory in home folder
mkdir -p /home/"$USER"/Git
cd Git/ || ( echo "That directory does not exist!" ; exit 1 ; )

# Personal Repository
# Clone personal repository from Gitlab
echo "Now cloning your personal repository from Gitlab"
git clone https://gitlab.com/anhvo8836/Arch &> /dev/null
echo "Your personal repository has been cloned into your home folder"
echo

# Arnix Repository
# Clone Arnix repository from Gitlab
echo "Now cloning your Arnix repository from Gitlab"
git clone https://gitlab.com/anhvo8836/Arnix &> /dev/null
echo "Your scripts repository has been cloned into your home folder"
echo

# Package Repository
# Clone package repository from Gitlab
echo "Now cloning your package repository from Gitlab"
git clone https://gitlab.com/anhvo8836/Packages &> /dev/null
echo "Your package repository has been cloned into your home folder"

# Navigate back to home folder
cd /home/"${USER}" || ( echo "That directory does not exist!" ; exit 1 ; )

