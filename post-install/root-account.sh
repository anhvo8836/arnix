#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

################
# ROOT ACCOUNT #
################
# Copy configuration files to the root account home folder

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "ROOT ACCOUNT CONFIGURATION FILES" 
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Root Account Shell
echo "The root account's Fish config file will now be updated"
sudo -s cp -r /home/"$USER"/.config/fish/ /root/.config/
echo "The root account's Starship prompt config file will now be updated"
sudo -s cp /home/"$USER"/.config/starship.toml /root/.config/starship.toml
echo

# Root Account HTop 
echo "The root account's Htop config file will be updated"
sudo -s cp -r /home/"$USER"/.config/htop/ /root/.config/

