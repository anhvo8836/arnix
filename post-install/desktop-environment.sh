#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

#######################
# DESKTOP ENVIRONMENT #
#######################
# Copy over desktop environment-related configuration files

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "==[ DESKTOP ENVIRONMENT ]=="
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Terminal
case $XDG_CURRENT_DESKTOP in

    KDE )

        # If /home/$USER/.local/share/konsole exists, remove it
        if [[ -d "/home/"${USER}"/.local/share/konsole" ]]; then 
        
            rm -rf /home/"${USER}"/.local/share/konsole
        
        fi

        echo "Your Konsole configuration files will now be copied to /home/${USER}/.local/share/konsole"
        ln -sf /home/"${USER}"/Git/Arch/.local/share/konsole /home/"${USER}"/.local/share/

    ;;

    GNOME )

    ;;

    XFCE )

    ;;

esac
