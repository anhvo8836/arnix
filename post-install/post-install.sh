#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

clear

# Start screen
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "==[ POST-INSTALLATION ]=="
fg_blue ; print_centered "=" "=" ; reset_colour
echo
print_centered "Hello, $USER"
echo
read -r | print_centered "Press [ ENTER ] to begin the post-installation process"

####################
# GIT REPOSITORIES #
####################
# Execute git.sh to clone your git repositories
sh /home/"${USER}"/post-install/git.sh

############################
# USER CONFIGURATION FILES #
############################
# Execute user-configuration-files.sh to copy config files from your personal repository
sh /home/"${USER}"/post-install/user-configuration-files.sh

###########################
# DEVELOPMENT ENVIRONMENT #
###########################
# Execute development-enviroment.sh to copy Neovim config files from your personal repository
sh /home/"${USER}"/post-install/development-environment.sh

#######################
# DESKTOP ENVIRONMENT #
#######################
# Execute desktop-environment.sh to copy over any relevant desktop environment configuration files and themes.
sh /home/"${USER}"/post-install/desktop-environment.sh

echo
fg_red ; print_centered "***** You will need your sudo password for the next section *****"
echo
sleep 1

################
# ROOT ACCOUNT #
################
# Execute root-account.sh to copy configuration files to the root account.
sh /home/"${USER}"/post-install/root-account.sh

##############################
# SYSTEM CONFIGURATION FILES #
##############################
# Execute system-configuration-files.sh to copy configuration files to the appropriate locations.
sh /home/"${USER}"/post-install/system-configuration-files.sh

###################
# CLEAN UP / MISC #
###################

# Create a symbolic link to the .scripts folder in your personal repository.
ln -sf /home/"${USER}"/Git/Arch/.scripts/ /home/"${USER}"

# Create output directory for ArchISO
mkdir -p /home/"${USER}"/ISOs/

# Remove Post-Install Script
sudo rm -rf /home/"${USER}"/post-install

# End Screen
echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "FINISHED"
fg_blue ; print_centered "=" "=" ; reset_colour
echo
print_centered "The post-install script has completed"
read -r | print_centered "Press [ ENTER ] to exit"
clear && exit

