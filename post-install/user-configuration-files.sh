#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

############################
# USER CONFIGURATION FILES #
############################
# Copy over config files from personal repository

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "USER CONFIGURATION FILES" 
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Terminal

echo "Your fish config file will now be updated"
ln -sf /home/"${USER}"/Git/Arch/.config/fish/config.fish /home/"${USER}"/.config/fish/config.fish

echo "Your Starship prompt config file will now be updated"
ln -sf /home/"${USER}"/Git/Arch/.config/starship.toml /home/"${USER}"/.config/starship.toml

# HTop 

echo "Your Htop config file will be updated"
ln -sf /home/"${USER}"/Git/Arch/.config/htop/ /home/"${USER}"/.config/

