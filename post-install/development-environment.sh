#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /home/"${USER}"/post-install/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /home/"${USER}"/post-install/functions.sh

###########################
# DEVELOPMENT ENVIRONMENT #
###########################
# Copy over neovim configuration files

echo
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "==[ DEVELOPMENT ENVIRONMENT ]=="
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Test to see if npm is installed on the system and install it if not.
if ! command -v npm &> /dev/null

then

    echo -e "${FG_L_RED}NPM is not installed on the system${END}"
    echo "It will now be installed"
    echo
    sudo pacman -S npm --needed --noconfirm --disable-download-timeout &> /dev/null
    echo -e "${FG_L_GREEN}NPM has been installed"

fi

# Install the Packer plugin manager
echo "The Packer plugin manager for Neovim will now be installed"
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim &> /dev/null

# Copy main neovim configuration folder to .config
echo "Your neovim config will now be updated"
echo "Remember to open Neovim and run :PackerSync to install your plugins and :Mason to install your LSPs"
cp -r /home/"${USER}"/Git/Arch/.config/nvim /home/"$USER"/.config/

