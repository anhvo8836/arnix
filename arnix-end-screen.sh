#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

##############
# END SCREEN #
##############
# You will be shown a final message indicating that Arch Linux has been installed and to reboot the system.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "ARNIX INSTALLER"
fg_blue ; print_centered "=" "=" ; reset_colour
echo
print_centered "Arch Linux has been installed and configured."
print_centered "Your system is ready to reboot."
print_centered "Remember to disconnect your installation media."
echo
read -r | print_centered "Press enter to reboot your system"
echo
clean_up
fg_red ; print_centered "Your system will now reboot" ; reset_colour
sleep 1;clear && reboot

