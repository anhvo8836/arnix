#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

#################
# USER ACCOUNTS #
#################
# This script will guide you through setting up your user and root accounts.

# Personal Account
# Set up your personal account by setting a name, password, and your default interactive shell
echo "What will the name of your user account be?"
echo -ne "Username ${ARROW}";read -r USERNAME
useradd -m "${USERNAME}"
passwd "${USERNAME}"
echo
echo "${USERNAME} has been created"
echo

# Change Default Interactive Shell
# Change your default interactive shell to FISH.
echo "${USERNAME}'s default interactive shell will be changed to fish"
echo -e "\n# Start Fish shell as the interactive shell\nexec fish" >> /home/"${USERNAME}"/.bashrc
echo

# Wheel Group
# Add your account to the wheel group for sudo privileges.
echo "${USERNAME} will be added to the wheel group"
sed -i '85s/^#//' /etc/sudoers
usermod -aG wheel "${USERNAME}"
echo

# Libvirtd Service
# This will enable kernel level virtualization support on the new system.
# This will only be enabled if you chose to install your virtualization toolkit.
case $VIRTUALIZATION_TOOLKIT in

    Y | y )

        echo -e "${USERNAME} will be added to the libvirt group"
        usermod -aG libvirt "${USERNAME}"
        echo

    ;;

    N | n )

    ;;

    * )

        echo "${USERNAME} will be added to the libvirt group"
        usermod -aG libvirt "${USERNAME}"
        echo

    ;;

esac

# Post-Installation Script
# Copy post-installation folder to new user's home.
echo "Your post-installation script will be moved to ${USERNAME}'s home folder"
cp -r /arnix/post-install /home/"${USERNAME}"

echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

# Root Account
# Set up the root account on the new system.
echo "Let's set the password for the root account."
passwd
echo

# Change Default Interactive Shell
echo "The root account's default interactive shell will be changed to fish"
cp /etc/skel/.bashrc /root
echo -e "\n# Start Fish shell as the interactive shell\nexec fish" >> /root/.bashrc
