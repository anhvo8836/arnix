#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

################
# REFIND SETUP #
################
# This script will be used to install and configure rEFInd to the EFI partition on the new system.
# Install rEFInd
echo "rEFInd will now be installed and configured"
refind-install &> /var/log/arnix/arnix.log
echo

# Configure Refind
# This function is your refind-setup script turned into a function.
# This will allow the function to be called from the main script as opposed to a seperate script.
# Usage : configure_refind

# Detect installed CPU
CPU=$(awk 'NR==5,/model name/ {print $4}' /proc/cpuinfo)

# Set the root parttion UUID as variable.
ROOT_UUID=$(blkid /dev/"${DEVICE}${ROOT}" -s UUID -o value)

# Set the localation of the microcode package as a variable.
case $CPU in
    
    AMD )

        CPU="AMD"
        MICROCODE="initrd=\\\amd-ucode.img"

    ;;

    * )

        CPU="Intel"
        MICROCODE="initrd=\\\intel-ucode.img"

    ;;

esac

# Set the location of the initramfs image as a variable.
INITRAMFS="initrd=\\\initramfs-${KERNEL}.img"
    
# Create the rEFInd configuration file
echo "The main rEFInd configuration file will now be created"
echo
echo "Root Parition : /dev/${DEVICE}${ROOT}"
echo "UUID : ${ROOT_UUID}"
echo "Detected CPU : ${CPU}"
echo

# touch /mnt/boot/refind_linux.conf
touch /boot/refind_linux.conf

case $FILESYSTEM in

    1 ) # EXT4

        echo -e '"Boot with standard options"  "root=UUID='${ROOT_UUID}' rw '${MICROCODE}' '${INITRAMFS}' loglevel=3 quiet"' > /boot/refind_linux.conf
        echo -e '"Boot with minimal options"   "ro root=UUID='${ROOT_UUID}'"' >> /boot/refind_linux.conf

    ;;

    2 ) # BTRFS

        echo -e '"Boot with standard options"  "root=UUID='${ROOT_UUID}' rw rootflags=subvol=@ '${MICROCODE}' '${INITRAMFS}' loglevel=3 quiet"' > /boot/refind_linux.conf
        echo -e '"Boot with minimal options"   "ro root=UUID='${ROOT_UUID}'"' >> /boot/refind_linux.conf

    ;;

    * ) # Default option

        echo -e '"Boot with standard options"  "root=UUID='${ROOT_UUID}' rw '${MICROCODE}' '${INITRAMFS}' loglevel=3 quiet"' > /boot/refind_linux.conf
        echo -e '"Boot with minimal options"   "ro root=UUID='${ROOT_UUID}'"' >> /boot/refind_linux.conf

    ;;

esac

# Disable Selction Timer
# This will let you stay on the boot screen until you choose an operating system to boot into.
echo "The selection timer will be disabled"
sed -i 's/timeout 20/timeout 0/' /boot/EFI/refind/refind.conf
echo

# Apply rEFInd Theme
echo "Your rEFInd theme will be applied"
mkdir -p /boot/EFI/refind/themes
cp -r /arnix/themes/refind-theme-regular /boot/EFI/refind/themes/refind-theme-regular
echo -e "\n# Include Theme\ninclude themes/refind-theme-regular/theme.conf" >> /boot/EFI/refind/refind.conf

