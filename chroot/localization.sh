#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

################
# LOCALIZATION #
################
# Timezone and Localization
# Set the systemi timezone, language and hardware clock.
echo "The system-wide timezone will be set to America/Toronto"
ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime

echo "The system-wide language will now be set to American English"
sed -i '171s/^#//' /etc/locale.gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen &> /var/log/arnix/arnix.log

echo "The system-wide clock will now be synced to the hardware clock on your motherboard"
timedatectl set-ntp true &> /var/log/arnix/arnix.log
timedatectl set-local-rtc true &> /var/log/arnix/arnix.log
hwclock --systohc &> /var/log/arnix/arnix.log
