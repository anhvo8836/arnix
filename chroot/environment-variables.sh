#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

#########################
# ENVIRONMENT VARIABLES #
#########################
# This script will set the system's environment variables.
case $DESKTOP in

    1 ) # KDE Plasma

        echo "The following environment variable(s) will be set"
        echo 'EDITOR="/usr/bin/nvim"'
        echo 'XDG_CURRENT_DESKTOP=KDE'
        echo -e EDITOR='"/usr/bin/nvim"' >> /etc/environment
        echo -e XDG_CURRENT_DESKTOP=KDE >> /etc/environment

    ;;

    2 ) # Gnome

        echo "The following environment variable(s) will be set"
        echo 'EDITOR="/usr/bin/nvim"'
        echo 'XDG_CURRENT_DESKTOP=GNOME'
        echo -e EDITOR='"/usr/bin/nvim"' >> /etc/environment
        echo -e XDG_CURRENT_DESKTOP=GNOME >> /etc/environment

    ;;

    3 ) # XFCE

        echo "The following environment variable(s) will be set"
        echo 'EDITOR="/usr/bin/nvim"'
        echo 'XDG_CURRENT_DESKTOP=XFCE'
        echo -e EDITOR='"/usr/bin/nvim"' >> /etc/environment
        echo -e XDG_CURRENT_DESKTOP=XFCE >> /etc/environment

    ;;

    * ) # Default option

        echo "The following environment variable(s) will be set"
        echo 'EDITOR="/usr/bin/nvim"'
        echo 'XDG_CURRENT_DESKTOP=KDE'
        echo -e EDITOR='"/usr/bin/nvim"' >> /etc/environment
        echo -e XDG_CURRENT_DESKTOP=KDE >> /etc/environment

    ;;

esac
