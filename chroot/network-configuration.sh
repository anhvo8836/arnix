#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

#########################
# NETWORK CONFIGURATION #
#########################
# Set a host for the system
echo "What should the hostname be for your new system?"
echo "This is the name your system will be seen as on your network so set a descriptive name"
echo "ie : my-archlinux-vm"
echo "Default hostname : archlinux"
echo -ne "${FG_GREEN}>${END} "; read -r HOSTNAME
echo

# If an empty string is passed as the ESP size, set the ESP size to 300MB
if test -z "${HOSTNAME}"; then
    HOSTNAME="archlinux"
fi


echo "The device's hostname will be set to ${HOSTNAME}"
echo -e "${HOSTNAME}" > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\t\t\tlocalhost\n127.0.1.1\t${HOSTNAME}" >> /etc/hosts
