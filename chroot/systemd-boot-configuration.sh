#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

######################
# SYSTEMD-BOOT SETUP #
######################
# This script will be used to install Systemd-boot to the EFI partition on the new system.

# Detect installed CPU
CPU=$(awk 'NR==5,/model name/ {print $4}' /proc/cpuinfo)

# Set Microcode package as a variable
if [[ $CPU = "AMD" ]]; then

    CPU="AMD"
    MICROCODE=amd-ucode.img

else

    CPU="Intel"
    MICROCODE=intel-ucode.img

fi

# Set the root parttion UUID as variable.
ROOT_PARTUUID=$(blkid /dev/"${DEVICE}${ROOT}" -s PARTUUID -o value)


# Install Systemd-boot
echo "Systemd-boot will now be installed to /boot/efi"
bootctl install &> /var/log/arnix/arnix.log
echo

# Create loader configuration file
echo "The loader configuration file will be created - /boot/efi/loader/loader.conf"
touch /boot/loader/loader.conf
echo "default  arch.conf" > /boot/loader/loader.conf
echo "timeout  4" >> /boot/loader/loader.conf 
echo "console-mode max" >> /boot/loader/loader.conf  
echo "editor   no" >> /boot/loader/loader.conf  
echo

# Display resulting file
cat /boot/loader/loader.conf
echo

# Create loader entry file
echo "The loader entry file will now be created - /boot/efi/loader/entries/arch.conf"
echo
echo "Root Parition : /dev/${DEVICE}${ROOT}"
echo "PARTUUID : ${ROOT_PARTUUID}"
echo "Detected CPU : ${CPU}"
echo
touch /boot/loader/entries/arch.conf  
case $FILESYSTEM in

    1 ) # EXT4

        echo -e "title   Arch Linux" > /boot/loader/entries/arch.conf 
        echo -e "linux   /vmlinuz-${KERNEL}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /${MICROCODE}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /initramfs-${KERNEL}.img" >> /boot/loader/entries/arch.conf 
        echo -e "options root=PARTUUID=${ROOT_PARTUUID} rw" >> /boot/loader/entries/arch.conf  

    ;;

    2 ) # BTRFS

        echo -e "title   Arch Linux" > /boot/loader/entries/arch.conf 
        echo -e "linux   /vmlinuz-${KERNEL}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /${MICROCODE}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /initramfs-${KERNEL}.img" >> /boot/loader/entries/arch.conf 
        echo -e "options root=PARTUUID=${ROOT_PARTUUID} rootflags=subvol=@ rw" >> /boot/loader/entries/arch.conf  

    ;;

    * ) # Default option

        echo -e "title   Arch Linux" > /boot/loader/entries/arch.conf 
        echo -e "linux   /vmlinuz-${KERNEL}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /${MICROCODE}" >> /boot/loader/entries/arch.conf 
        echo -e "initrd  /initramfs-${KERNEL}.img" >> /boot/loader/entries/arch.conf 
        echo -e "options root=PARTUUID=${ROOT_PARTUUID} rw" >> /boot/loader/entries/arch.conf  
    
    ;;

esac

# Display resulting file
# This is only meant for testing purposes and will be removed after successful testing
cat /boot/loader/entries/arch.conf
