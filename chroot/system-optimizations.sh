#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

########################
# SYSTEM OPTIMIZATIONS #
########################
# System Optimization
# Set up swap space, reflector, and parallel compiling.
# Enabling parallel compiling with allowing for the CPU to use all cores available when building packages from source.

# Set up swap space (zRAM)
# This will set up swap space using zRAM
# The size of the swap space will be equal to half (1/2) of the available physical memory on the system.
echo "Swap space (zram) will be configured"
touch /etc/systemd/zram-generator.conf
echo -e "[zram0]\nzram-fraction=0.5\nmax-zram-size=4096" > /etc/systemd/zram-generator.conf
systemctl daemon-reload &> /var/log/arnix/arnix.log
systemctl start /dev/zram0 &> /var/log/arnix/arnix.log

# Reflector Service
# This will configure the options used when Reflector is run
# --country : The country/countries in which mirrors will be pulled from
# --latest : The number of most recent mirrors
# --sort : Sort the mirrorlist by order of speed
echo "Reflector will be configured to run at boot and refresh your mirrorlist"
sed -i 's/--country France,Germany/--country Canada,United States/' /etc/xdg/reflector/reflector.conf
sed -i 's/--latest 5/--latest 25/' /etc/xdg/reflector/reflector.conf
sed -i 's/--sort age/--sort rate/' /etc/xdg/reflector/reflector.conf

# Parallel Compiling
# Enable parallel compiling on the new system
# Your system will be set up to use all available cores when compiling packages.
# This will greatly speed up the process of builsing packages from source or the AUR.
echo "Parallel compiling will be enabled"
sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/' /etc/makepkg.conf
