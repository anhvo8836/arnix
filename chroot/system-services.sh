#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source any other files that are needed here.
source /arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /arnix/functions.sh

###################
# SYSTEM SERVICES #
###################
# System Services
# Enable the various system services that will be needed.

# Reflector
# This will refresh your mirrorlist with every system (re)boot.
echo "The Reflector service will be enabled"
systemctl enable reflector.service &> /var/log/arnix/arnix.log

# FSTrim
# This will periodically clean your hard drive.
echo "The FStrim timer will be enabled"
systemctl enable fstrim.timer &> /var/log/arnix/arnix.log

# Network Manager
# This will allow the system to communicate with the internet.
# It will also for easy configuration and troubleshooting, if needed.
echo "The NetworkManager service will be enabled"
systemctl enable NetworkManager.service &> /var/log/arnix/arnix.log

# Display Manager
# This will give you a login screen
case $DESKTOP in

    1 ) # KDE - SDDM

        echo "The SDDM display manager service will now be enabled"
        systemctl enable sddm.service &> /var/log/arnix/arnix.log

    ;;

    2 ) # Gnome - GDM

        echo "The GDM display manager service will now be enabled"
        systemctl enable gdm.service &> /var/log/arnix/arnix.log

    ;;

    3 ) # XFCE - LightDM

        echo "The LightDM display manager service will now be enabled"
        systemctl enable lightdm.service &> /var/log/arnix/arnix.log

    ;;

    * ) # KDE - SDDM

        echo "The SDDM display manager service will now be enabled"
        systemctl enable sddm.service &> /var/log/arnix/system-services.log

    ;;


esac

# Printer Service
# This will enable printer support on the new system.
case $PRINTERS in

    Y | y )

        echo -e "Printer services will now be enabled"
        systemctl enable cups.service &> /var/log/arnix/system-services.log

    ;;

    N | n )

    ;;

    * )

        echo -e "Printer services will now be enabled"
        systemctl enable cups.service &> /var/log/arnix/system-services.log

    ;;

esac

# Libvirtd
# This is required for your virtualization toolkit.
case $VIRTUALIZATION_TOOLKIT in

    Y | y )

        echo -e "The libvirtd service will now be enabled"
        systemctl enable libvirtd.service &> /var/log/arnix/system-services.log

    ;;

    N | n )

    ;;

    * )

        echo -e "The libvirtd service will now be enabled"
        systemctl enable libvirtd.service &> /var/log/arnix/system-services.log

    ;;

esac

