#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

###########################
# DISK PREPARATION - EXT4 #
###########################
# We will use this script to create the appropriate partitions on the drive you selected.
# Create the EFI, root, and home partitions.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "DISK PREPARATION - EXT4"
fg_blue ; print_centered "=" "=" ; reset_colour
echo
echo "A ESP ($ESP_SIZE) will now be created on /dev/$DEVICE"
echo "A Root partition ($ROOT_SIZE) will now be created on /dev/$DEVICE"
echo "A Home partition (remaining space) will now be created on /dev/$DEVICE"
(echo "g" ; echo "n" ; echo "" ; echo "" ; echo "+$ESP_SIZE" ; echo "t" ; echo "1" ; echo "n" ; echo "" ; echo "" ; echo "+$ROOT_SIZE" ; echo "n" ; echo "3" ; echo "" ; echo "" ; echo "w") | fdisk -W always /dev/$DEVICE &> /dev/null
echo

# Set partitions as variables.
# This will enable the script to format and mount the partitions as needed without the need for user input.
case $DEVICE_TYPE in

    1 ) # Drive Type : NVME

        case $FILESYSTEM in

            1 )

                ESP="p1"
                ROOT="p2"
                HOME="p3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

            2 )

            ;;

            * )

                ESP="p1"
                ROOT="p2"
                HOME="p3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

        esac

    ;;

    2 ) # Drive Type : Other

        case $FILESYSTEM in

            1 ) # EXT4

                ESP="1"
                ROOT="2"
                HOME="3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

            2 ) # BTRFS

            ;;

            * ) # Default option

                ESP="1"
                ROOT="2"
                HOME="3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

        esac

    ;;

    * ) # Default option

        case $FILESYSTEM in

            1 ) # EXT4

                ESP="p1"
                ROOT="p2"
                HOME="p3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

            2 ) # BTRFS

            ;;

            * ) # EXT4

                ESP="p1"
                ROOT="p2"
                HOME="p3"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo "/dev/${DEVICE}${HOME} has been set as the home partition"
                echo

            ;;

        esac

    ;;

esac

# Format Partitions
# Format the newly created partitions into the appropriate filesystems.
echo "/dev/${DEVICE}${ESP} will now be formatted to FAT32"
mkfs.fat -F 32 -n "ARCH-BOOT" /dev/"${DEVICE}${ESP}" &> /dev/null

echo "/dev/${DEVICE}${ROOT} will now be formatted to EXT4"
mkfs.ext4 -L "ROOT" /dev/"${DEVICE}${ROOT}" &> /dev/null

echo "/dev/${DEVICE}${HOME} will now be formatted to EXT4"
mkfs.ext4 -L "HOME" /dev/"${DEVICE}${HOME}" &> /dev/null
echo

# Mount Partitions
# Mount Root and Home partitions
echo "/dev/${DEVICE}${ROOT} will be mounted to /mnt"
mount /dev/"${DEVICE}${ROOT}" /mnt

echo "/dev/${DEVICE}${HOME} will be mounted to /mnt/home"
mkdir -p /mnt/home
mount /dev/"${DEVICE}${HOME}" /mnt/home

echo "/dev/${DEVICE}${ESP} will be mounted to /mnt/boot"
mkdir -p /mnt/boot
mount /dev/"${DEVICE}${ESP}" /mnt/boot

# Export Variables
export DEVICE_TYPE
export DEVICE
export ESP
export ROOT
export HOME
