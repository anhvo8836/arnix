#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

############################
# DISK PREPARATION - BTRFS #
############################
# We will use this script to create the appropriate partitions on the drive you selected.
# Create the EFI and root partitions.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "DISK PREPARATION - BTRFS"
fg_blue ; print_centered "=" "=" ; reset_colour
echo
echo "A ESP ${ESP_SIZE} will now be created on /dev/${DEVICE}"
echo "A Root partition (remaining space) will now be created on /dev/${DEVICE}"
(echo "g" ; echo "n" ; echo "" ; echo "" ; echo "+${ESP_SIZE}" ; echo "t" ; echo "1" ; echo "n" ; echo "" ; echo "" ; echo "" ; echo "w") | fdisk -W always /dev/"${DEVICE}" &> /dev/null
echo

# Set partitions as variables.
# This will enable the script to format and mount the partitions as needed without the need for user input.
case $DEVICE_TYPE in

    1 ) # Drive Type : NVME

        case $FILESYSTEM in

            1 )


            ;;

            2 )

                ESP="p1"
                ROOT="p2"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo

            ;;

            * )

            ;;

        esac

    ;;

    2 ) # Drive Type : Other

        case $FILESYSTEM in

            1 ) # EXT4

            ;;

            2 ) # BTRFS

                ESP="1"
                ROOT="2"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo



            ;;

            * ) # Default option

            ;;

        esac

    ;;

    * ) # Default option

        case $FILESYSTEM in

            1 ) # EXT4


            ;;

            2 ) # BTRFS

                ESP="p1"
                ROOT="p2"

                echo "/dev/${DEVICE}${ESP} has been set as the ESP"
                echo "/dev/${DEVICE}${ROOT} has been set as the root partition"
                echo


            ;;

            * ) # EXT4

            ;;

        esac

    ;;

esac

# Format Partitions
# Format the newly created partitions into the appropriate filesystems.
echo "/dev/${DEVICE}${ESP} will now be formatted to FAT32"
mkfs.fat -F 32 -n "ARCH-BOOT" /dev/"${DEVICE}${ESP}" &> /dev/null

echo "/dev/${DEVICE}${ROOT} will now be formatted to BTRFS"
mkfs.btrfs -f -L "ROOT" /dev/"${DEVICE}${ROOT}" &> /dev/null
echo

# Mount Partitions
# Create and Mount Subvolumes
# This will create a subvolume at / (@), /var (@var), and /home (@home) on the main BTRFS partition. The script will also mount the subvolumes to the appropriate mount points.

echo "The following subvolumes will now be created and mounted to /dev/${DEVICE}${ROOT} : '@' '@var' '@home'"
mount /dev/"${DEVICE}${ROOT}" /mnt
btrfs su cr /mnt/@ &> /dev/null
btrfs su cr /mnt/@var &> /dev/null
btrfs su cr /mnt/@home &> /dev/null

echo "The subvolumes will now be mounted : '@ -> /mnt' '@var -> /mnt/var' '@home -> /mnt/home'"
umount /mnt
mount -o defaults,noatime,compress=zstd,commit=120,subvol=@ /dev/"${DEVICE}${ROOT}" /mnt
mkdir -p /mnt/{var,home}
mount -o defaults,noatime,compress=zstd,commit=120,subvol=@var /dev/"${DEVICE}${ROOT}" /mnt/var
mount -o defaults,noatime,compress=zstd,commit=120,subvol=@home /dev/"${DEVICE}${ROOT}" /mnt/home

echo "/dev/${DEVICE}${ESP} will be mounted to /mnt/boot"
mkdir -p /mnt/boot
mount /dev/"${DEVICE}${ESP}" /mnt/boot

# Export Variables
export DEVICE_TYPE
export DEVICE
export ESP
export DEVICE
export ROOT
