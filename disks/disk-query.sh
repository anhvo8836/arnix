#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

############################
# DISK PREPARATION - QUERY # 
############################
# This script will guide you through picking a disk to install Arch Linux to.
# You will also be guided through picking a file format and appropriate partition sizes.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "DISK PREPERATION - QUERY"
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Drive Type
# You will be promptedSelect the appropriate type of drive Arch Linux will be installed to. 
# This will help set partitions later on.
echo "What type of drive will you be installing Arch Linux to?"
echo -e "${BOLD}${FG_BLUE}1) NVME (Default)${END}"
echo "2) Other"
echo -ne "$ARROW" ; read -r DEVICE_TYPE
echo

# Drive Selection
# List all drives connected to the system and pick the one Arch inux will be installed to.
lsblk -f
echo
echo -ne 'Which drive will you be installing Arch Linux to? > /dev/';read -r DEVICE
echo "Arch Linux will be installed to /dev/$DEVICE"
echo

# Choose File System
# Create applicable partitions depending on chosen filesystem
echo "What filesystem would you like to use"
echo -e "${BOLD}${FG_BLUE}1) EXT4 (Default)${END}"
echo "2) BTRFS"
echo -ne "$ARROW" ; read -r FILESYSTEM
echo

# Partition Size
# Determine the size of the applicable partitions
case $FILESYSTEM in

    1 ) # EXT4

        echo "How big should the ESP be?"
        echo "Ex. Enter 300M to create a 300MB ESP"
        echo "NOTE : If left blank, the default ESP size will be set to 300MB as recommended by the Arch Linux installation guide"
        echo -ne "$ARROW" ; read -r ESP_SIZE
        echo

        # If an empty string is passed as the ESP size, set the ESP size to 300MB
        if test -z "$ESP_SIZE"; then
            ESP_SIZE="300M"
        fi

        echo "How big should the root partition be?"
        echo "Ex. Enter 20G to create a 20GB root partition"
        echo "NOTE : If left blank, the root partition will be set to 20GB and the remainder of the drive will be allocated to your home partition"
        echo -ne "$ARROW" ; read -r ROOT_SIZE
        echo

        # If an empty string is passed as the root partition size, set the root partition size to 20GB. The rest will then be allocated to the home partition.
        if test -z "$ROOT_SIZE"; then
            ROOT_SIZE="20G"
        fi

    ;;

    2 ) # BTRFS

        echo "How big should the ESP be?"
        echo "Ex. Enter 300M to create a 300MB ESP"
        echo "NOTE : If left blank, the default ESP size will be set to 300MB as recommended by the Arch Linux installation guide"
        echo -ne "$ARROW" ; read -r ESP_SIZE
        echo

        # If an empty string is passed as the ESP size, set the ESP size to 300MB
        if test -z "$ESP_SIZE"; then
            ESP_SIZE="300M"
        fi

    ;;

    * ) # EXT4

        echo "How big should the ESP be?"
        echo "Ex. Enter 300M to create a 300MB ESP"
        echo "NOTE : If left blank, the default ESP size will be set to 300MB as recommended by the Arch Linux installation guide"
        echo -ne "$ARROW" ; read -r ESP_SIZE
        echo

        # If an empty string is passed as the ESP size, set the ESP size to 300MB
        if test -z "$ESP_SIZE"; then
            ESP_SIZE="300M"
        fi

        echo "How big should the root partition be?"
        echo "Ex. Enter 20G to create a 20GB root partition"
        echo "NOTE : If left blank, the root partition will be set to 20GB and the remainder of the drive will be allocated to your home partition"
        echo -ne "$ARROW" ; read -r ROOT_SIZE
        echo 

        # If an empty string is passed as the root partition size, set the root partition size to 20GB. The rest will then be allocated to the home partition.
        if test -z "$ROOT_SIZE"; then
            ROOT_SIZE="20G"
        fi

    ;;

esac

# Export Variables
export DEVICE_TYPE
export DEVICE
export ESP_SIZE
export ROOT_SIZE
export FILESYSTEM
