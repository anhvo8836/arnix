#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

####################################
# INSTALLATION PREPARATION - QUERY #
####################################
# You will be asked several questions to determine what desktop applications gets installed.

# Browser - Firefox
# You will be asked if you want to install Firefox.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install Firefox?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r BROWSER
echo

# Email Client - Thunderbird
# You will be asked if you want to install Thunderbird.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install Thunderbird?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r EMAIL_CLIENT
echo

# Office Suite - LibreOffice
# You will be asked if you want to install LibreOffice.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install LibreOffice?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r OFFICE_SUITE
echo

# Video Player = VLC 
# You will be asked if you want to install VLC.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install VLC?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r VIDEO_PLAYER
echo

# Messenger Application - Caprine 
# You will be asked if you want to install Caprine.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install Caprine?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r MESSENGER
echo

# Export Variables
# Variables will be exported so that they can be used during the pacstrap stage
export BROWSER
export EMAIL_CLIENT
export OFFICE_SUITE
export VIDEO_PLAYER
export MESSENGER
