#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source the pacstrap-query script to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

#################
# BASE PACKAGES #
#################
# Install base packages
echo "The base system will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}base"
echo "linux"
echo "linux-headers"
echo -e "linux-firmware${END}"
pacstrap /mnt base "${KERNEL}" "${KERNEL_HEADERS}" linux-firmware --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
