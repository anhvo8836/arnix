#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

#######################
# EXTRA BASE PACKAGES #
#######################
# Install extra base packages
case $FILESYSTEM in

    1 ) # EXT4

        echo "Extra base packages will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}base-devel"
        echo "pacman-contrib"
        echo "man-pages"
        echo "man-db"
        echo "reflector"
        echo "zram-generator"
        echo -e "htop${END}"
        pacstrap /mnt base-devel pacman-contrib man-pages man-db reflector zram-generator htop --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    2 ) # BTRFS

        echo "Extra base packages will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}base-devel"
        echo "btrfs-progs"
        echo "pacman-contrib"
        echo "man-pages"
        echo "man-db"
        echo "reflector"
        echo "zram-generator"
        echo -e "htop${END}"
        pacstrap /mnt base-devel btrfs-progs pacman-contrib man-pages man-db reflector zram-generator htop --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    * ) # Default optiom

        echo "Extra base packages will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}base-devel"
        echo "pacman-contrib"
        echo "man-pages"
        echo "man-db"
        echo "reflector"
        echo "zram-generator"
        echo -e "htop${END}"
        pacstrap /mnt base-devel pacman-contrib man-pages man-db reflector zram-generator htop --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac
