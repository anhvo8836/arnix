#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

#######################
# DEVELOPMENT TOOLKIT #
#######################
# Install your development toolkit
# If you chose not to install the toolkit, Neovim will be installed on the system as your text editor.
case $DEVELOPMENT_TOOLKIT in

    Y | y )

        echo "Your development toolkit will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}archiso"
        echo "neovim"
        echo "git"
        echo "git-lfs"
        echo -e "npm${END}"
        pacstrap /mnt archiso neovim git git-lfs npm --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

        echo "You chose not to install your development toolkit"
        echo "Neovim will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}neovim${END}" 
        pacstrap /mnt neovim --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    * )

        echo "Your development toolkit will now be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}archiso"
        echo "neovim"
        echo "git"
        echo "git-lfs"
        echo -e "npm${END}"
        pacstrap /mnt archiso neovim git git-lfs npm --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac
