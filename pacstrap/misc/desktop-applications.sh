#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

########################
# DESKTOP APPLICATIONS #
########################
# Install basic desktop applications
# This script will install the desktop applications you had chosen on the new system.

###########
# BROWSER #
###########
# Install Firefox if needed
case $BROWSER in

    Y | y )

        echo "Firefox will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}firefox${END}"
        pacstrap /mnt firefox --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

    ;;

    * )

        echo "Firefox will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}firefox${END}"
        pacstrap /mnt firefox --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac

################
# EMAIL CLIENT #
################
# Install Thunderbird if needed
case $EMAIL_CLIENT in

    Y | y )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "Thunderbird will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}thunderbird${END}"
        pacstrap /mnt thunderbird --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

    ;;

    * )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "Thunderbird will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}thunderbird${END}"
        pacstrap /mnt thunderbird --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac

################
# OFFICE SUITE #
################
# Install LibreOffice if needed
case $OFFICE_SUITE in

    Y | y )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "LibreOffice will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}libreoffice${END}"
        pacstrap /mnt libreoffice --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

    ;;

    * )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "LibreOffice will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}libreoffice-fresh${END}"
        pacstrap /mnt libreoffice-fresh --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac

################
# VIDEO PLAYER #
################
# Install VLC if needed
case $VIDEO_PLAYER in

    Y | y )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "VLC will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}vlc${END}"
        pacstrap /mnt vlc --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

    ;;

    * )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "VLC will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}vlc${END}"
        pacstrap /mnt vlc --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac

#############
# MESSENGER #
#############
# Install Caprine if needed
case $MESSENGER in

    Y | y )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "Caprine will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}caprine${END}"
        pacstrap /mnt caprine --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    N | n )

    ;;

    * )

        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo
        echo "Caprine will now be installed on the system"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}caprine${END}"
        pacstrap /mnt caprine --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac
