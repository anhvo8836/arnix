#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

##############
# NETWORKING #
##############
# Install packages needed for networking
# This case statement will be used to determine whether iptables should be ignored during installation.
# If you choose to install your virtualization toolkit, the profile will install iptables-nft, which will conflict
# with iptables provided by networkmanager. 
echo "The appropriate network packages will now be installed to configure your network"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}networkmanager"
echo -e "network-manager-applet${END}"
pacstrap /mnt networkmanager network-manager-applet --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check

