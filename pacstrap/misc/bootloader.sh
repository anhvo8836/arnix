#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

##############
# BOOTLOADER #
##############
# This sccript will install your chosen bootloader, efibootmgr, and the appropriate microcode package.

# Detect installed CPU
CPU=$(awk 'NR==5,/model name/ {print $4}' /proc/cpuinfo)

# Install bootloader and microcode package
echo "The appropiate packages will now be downloaded to install the bootloader"
echo
echo -e "${PACKAGES_HEADER}"
echo
case $BOOTLOADER in

    1 ) # GRUB

        case $FILESYSTEM in

            1 ) # EXT4

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "amd-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "intel-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

            2 ) # BTRFS

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo "amd-ucode"
                        echo -e "grub-btrfs${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode grub-btrfs --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo "intel-ucode"
                        echo -e "grub-btrfs${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode grub-btrfs --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

            * ) # Default option

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "amd-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "intel-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

        esac

    ;;

    2 ) # Systemd-Boot

        case $CPU in

            AMD )

                echo -e "${FG_L_CYAN}efibootmgr"
                echo -e "amd-ucode${END}"
                pacstrap /mnt efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
 
            ;;

            * )

                echo -e "${FG_L_CYAN}efibootmgr"
                echo -e "intel-ucode${END}"
                pacstrap /mnt efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

            ;;

        esac

    ;;

    3 ) # rEFInd

        case $CPU in

            AMD )

                echo -e "${FG_L_CYAN}refind"
                echo "efibootmgr"
                echo -e "amd-ucode${END}"
                pacstrap /mnt refind efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
 
            ;;

            * )

                echo -e "${FG_L_CYAN}refind"
                echo "efibootmgr"
                echo -e "intel-ucode${END}"
                pacstrap /mnt refind efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

            ;;

        esac

    ;;

    * ) # Default option

        case $FILESYSTEM in

            1 ) # EXT4

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "amd-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "intel-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

            2 ) # BTRFS

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo "amd-ucode"
                        echo -e "grub-btrfs${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode grub-btrfs --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo "intel-ucode"
                        echo -e "grub-btrfs${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode grub-btrfs --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

            * ) # Default option

                case $CPU in

                    AMD )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "amd-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr amd-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                    * )

                        echo -e "${FG_L_CYAN}grub"
                        echo "os-prober"
                        echo "efibootmgr"
                        echo -e "intel-ucode${END}"
                        pacstrap /mnt grub os-prober efibootmgr intel-ucode --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log

                    ;;

                esac

            ;;

        esac

    ;;
esac
error_check
