#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

#########################
# TERMINAL ENHANCEMENTS #
#########################
# Install terminal enhancement packages
echo "Terminal enhancement packages will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}noto-fonts"
echo "noto-fonts-emoji"
echo "ttf-firacode-nerd"
echo "fish"
echo "starship"
echo "lsd"
echo -e "bat${END}"
pacstrap /mnt noto-fonts noto-fonts-emoji ttf-firacode-nerd fish starship lsd bat --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check

