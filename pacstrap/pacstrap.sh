#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

################
# INSTALLATION #
################
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "INSTALLATION"
fg_blue ; print_centered "=" "=" ; reset_colour
echo

###############
# BASE SYSTEM #
###############
# Execute a script to install the base system.
sh /root/arnix/pacstrap/base/base.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#######################
# BASE SYSTEM - EXTRA #
#######################
# Execute a script to install extra base packages.
sh /root/arnix/pacstrap/base/base-extra.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#########################
# TERMINAL ENHANCEMENTS #
#########################
# Execute a script to install terminal enhancement packages.
sh /root/arnix/pacstrap/misc/terminal-enhancements.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#######################
# DEVELOPMENT TOOLKIT #
#######################
# Execute a script to install your development toolkit (if needed).
sh /root/arnix/pacstrap/toolkits/development.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##############
# NETWORKING #
##############
# Execute a script to install networking packages.
sh /root/arnix/pacstrap/misc/networking.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#################
# VIDEO DRIVERS #
#################
# Execute the apprropriate script to install your chosen video drivers.
case $VIDEO in

    1 )

        sh /root/arnix/pacstrap/video/open-source.sh

    ;;

    2 )
 
        sh /root/arnix/pacstrap/video/amd.sh

    ;;

    3 )

        sh /root/arnix/pacstrap/video/intel.sh

    ;;

    4 )

        sh /root/arnix/pacstrap/video/nvidia.sh

    ;;

    5 )

        sh /root/arnix/pacstrap/video/virtualbox-vmware.sh

    ;;

    * )

        sh /root/arnix/pacstrap/video/open-source.sh

    ;;

esac
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

################
# AUDIO SERVER #
################
# Execute the apprropriate script to install your chosen audio server.
case $AUDIO in

    1 )

        sh /root/arnix/pacstrap/audio/pipewire.sh

    ;;

    2 )

        sh /root/arnix/pacstrap/audio/pulseaudio.sh

    ;;

    * )
    
        sh /root/arnix/pacstrap/audio/pipewire.sh

    ;;

esac
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

###################
# PRINTER SUPPORT #
###################
# execute a script to install printer support packages (if needed).
case $PRINTERS in

    Y | y )

        sh /root/arnix/pacstrap/misc/printer-support.sh
        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo 
    ;;

    N | n )

    ;;

    * )

        sh /root/arnix/pacstrap/misc/printer-support.sh
        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo 

    ;;

esac

##########################
# VIRTUALIZATION TOOLKIT #
##########################
# Execute a script to install your virtualization toolkit (if needed).
case $VIRTUALIZATION_TOOLKIT in

    Y | y )

        sh /root/arnix/pacstrap/toolkits/virtualization.sh
        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo

    ;;

    N | n )

    ;;

    * )

        sh /root/arnix/pacstrap/toolkits/virtualization.sh
        echo
        fg_blue ; print_centered "-" "-" ; reset_colour
        echo

    ;;

esac


###########
# DESKTOP #
###########
# Execute the apprropriate script to install your chosen desktop environment.
case $DESKTOP in

    1 )

        sh /root/arnix/pacstrap/desktops/kde.sh

    ;;

    2 )

        sh /root/arnix/pacstrap/desktops/gnome.sh

    ;;

    3 )

        sh /root/arnix/pacstrap/desktops/xfce.sh

    ;;

    * )

        sh /root/arnix/pacstrap/desktops/kde.sh

    ;;

esac

########################
# DESKTOP APPLICATIONS #
########################
# Execute a script to install various desktop applications.
sh /root/arnix/pacstrap/misc/desktop-applications.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##############
# AUR HELPER #
##############
# Execute a script to install AUR helper tools.
sh /root/arnix/pacstrap/misc/aur-helper.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

#############
# TIMESHIFT #
#############
# Execute a script to install Timeshift.
sh /root/arnix/pacstrap/misc/timeshift.sh
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##############
# BOOTLOADER #
##############
# Execute a script to install your chose bootloader and the appropriate microcode package.
sh /root/arnix/pacstrap/misc/bootloader.sh

