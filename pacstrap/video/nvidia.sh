#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
# We will also source the pacstrap-query script to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

# Install Nvidia video drivers
case $NVIDIA in

    1 ) # Open Kernel Module

        echo "The nVidia open kernel modules will be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}nvidia-open${END}"
        pacstrap /mnt nvidia-open --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    2 ) # Open-Source Nouveau Drivers

        echo "The Open Source Nouveau will be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG__L_CYAN}mesa"
        echo "xf86-video-nouveau"
        echo -e "libva-mesa-driver${END}"
        pacstrap /mnt mesa xf86-video-nouveau libva-mesa-driver --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log        
        error_check

    ;;

    3 ) # Proprietary Drivers

        echo "The Proprietary nVidia video drivers will be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo "${FG_L_CYAN}nvidia${END}"
        pacstrap /mnt nvidia --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

    * ) # Default option

        echo "The nVidia open kernel modules will be installed"
        echo
        echo -e "${PACKAGES_HEADER}"
        echo
        echo -e "${FG_L_CYAN}nvidia-open${END}"
        pacstrap /mnt nvidia-open --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
        error_check

    ;;

esac

