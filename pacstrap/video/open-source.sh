#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

# Install AMD video drivers
echo "Open-source video drivers will be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}mesa"
echo "xf86-video-amdgpu"
echo "xf86-video-ati"
echo "xf86-video-nouveau"
echo "xf86-video-vmware"
echo "libva-mesa-driver"
echo "libva-intel-driver"
echo "intel-media-driver"
echo "vulkan-radeon"
echo -e "vulkan-intel${END}"
pacstrap /mnt mesa xf86-video-amdgpu xf86-video-ati xf86-video-nouveau xf86-video-vmware libva-mesa-driver libva-intel-driver intel-media-driver vulkan-radeon vulkan-intel --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
