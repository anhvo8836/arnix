#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

# Install AMD video drivers
echo "The AMD video drivers will be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}mesa"
echo "xf86-video-amdgpu"
echo "xf86-video-ati"
echo "libva-mesa-driver"
echo -e "vulkan-radeon${END}"
pacstrap /mnt mesa xf86-video-amdgpu libva-mesa-driver vulkan-radeon --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log 
error_check
