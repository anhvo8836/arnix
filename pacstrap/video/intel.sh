#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

# Install Intel video drivers
echo "The Intel video drivers will be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}mesa"
echo "libva-intel-driver"
echo "intel-media-driver"
echo -e "vulkan-intel${END}"
pacstrap /mnt mesa libva-intel-driver intel-media-driver vulkan-intel --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
