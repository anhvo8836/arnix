#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

###################
# DESKTOP - GNOME #
###################
# Install Xorg
sh /root/arnix/pacstrap/desktops/xorg.sh

# Install GNOME
echo "The Gnome desktop environment will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}gnome"
echo -e "gdm${END}"
pacstrap /mnt gnome gnome-terminal gdm --needed --noconfirm --disable-download-timeout --ignore gnome-console &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

########################################
# DESKTOP - Additional Userspace Tools #
########################################
# Install additional userspace tools
echo "Additional userspace packages will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}gnome-tweaks"
echo "xdg-utils"
echo -e "xdg-desktop-portal-gnome${END}"
pacstrap /mnt gnome-tweaks xdg-utils xdg-desktop-portal-gnome --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo
