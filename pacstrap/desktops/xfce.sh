#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

##################
# DESKTOP - XFCE #
##################
# Install Xorg
sh /root/arnix/pacstrap/desktops/xorg.sh

# Install XFCE
echo "The XFCE desktop will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}xfce4"
echo "xfce4-goodies"
echo "lightdm"
echo -e "lightdm-gtk-greeter${END}" 
pacstrap /mnt xfce4 lightdm lightdm-gtk-greeter --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

########################################
# DESKTOP - Additional Userspace Tools #
########################################
# Install additional userspace tools
echo "Additional userspace packages will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}gvfs" 
echo "xdg-desktop-portal-gnome"
echo "xdg-utils"
echo -e "xarchiver${END}"
pacstrap /mnt gvfs xdg-desktop-portal-gnome xdg-utils xarchiver --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##################################
# DESKTOP - Additional GUI Tools #
##################################
# Install additional GUI tools
echo "Additional GUI tools will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}pavucontrol${END}"
pacstrap /mnt pavucontrol --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo
