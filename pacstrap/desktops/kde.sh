#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

########################
# DESKTOP - KDE PLASMA #
########################
# Install Xorg
sh /root/arnix/pacstrap/desktops/xorg.sh

# Install KDE Plasma
echo "The Plasma desktop will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}plasma"
echo "plasma-wayland-session"
echo "dolphin"
echo "konsole"
echo -e "sddm${END}"
pacstrap /mnt plasma plasma-wayland-session dolphin konsole sddm --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

##################################
# DESKTOP - Additional GUI Tools #
##################################
# Install additional GUI tools
echo "Various KDE-centric applications will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}kvantum"
echo -e "kdeconnect${END}"
pacstrap /mnt kvantum kdeconnect --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

########################################
# DESKTOP - Additional Userspace Tools #
########################################
# Install additional userspace tools
echo "Additional userspace packages will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}ark"
echo "xdg-desktop-portal-kde"
echo -e "xdg-utils${END}"
pacstrap /mnt ark xdg-desktop-portal-kde xdg-utils --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo
