#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

########
# XORG #
########
# Install Xorg
echo "The neccessary Xorg components will now be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}xorg-server"
echo -e "xorg-xinit${END}"
pacstrap /mnt xorg-server xorg-xinit --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

