#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

####################################
# INSTALLATION PREPARATION - Setup #
####################################
# This script will perform various tasks to setup the installation environment.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "INSTALLATION PREPERATION - SETUP"
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Mirrorlist
# Refresh the mirrorlist and populate it with a sorted list of the fastest mirrors available in Canada and America.
refresh_mirrors
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

# Pacman Confirguration
# Configure pacman by editing the /etc/pacman.conf file. 
# Color, Parallel Downloads, and the Multilib repository will be enabled during this step.
pacman_mods
echo
fg_blue ; print_centered "-" "-" ; reset_colour
echo

# Refresh Databases and Arch Linux keyring
# Refresh all of the repository databases as well as the Arch Linux keyring. 
# Refreshing the keyring will help prevent package from not installing due to PGP key issues.
refresh
