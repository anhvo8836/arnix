#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

###########################
# AUDIO SERVER - PIPEWIRE #
###########################
# Install Pipewire
echo "Pipewire will be installed"
echo
echo -e "${PACKAGES_HEADER}"
echo
echo -e "${FG_L_CYAN}pipewire"
echo "wireplumber"
echo "pipewire-pulse" 
echo "pipewire-alsa"
echo "pipewire-jack"
echo "gst-plugin-pipewire"
echo -e "libpulse${END}"
pacstrap /mnt pipewire wireplumber pipewire-pulse pipewire-alsa pipewire-jack --needed --noconfirm --disable-download-timeout &> /var/log/arnix/arnix.log
error_check
