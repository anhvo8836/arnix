#!/bin/bash

################################################################
#      __      __                                              #
#     /\ \    / /    Contact Info                              #
#    /  \ \  / /     Name : Anh Vo                             #
#   / /\ \ \/ /      Email : anhthevo.1989@gmail.com           #
#  / ____ \  /       Gitlab : https://www.gitlab.com/anhvo8836 #
# /_/    \_\/                                                  #
#                                                              #
################################################################

#############
# VARIABLES #
#############
# In this section, we will source our variables file to use its variables.
source /root/arnix/variables.sh

#############
# FUNCTIONS #
#############
# In this section, we will source our functions file so that we can access its the functions.
source /root/arnix/functions.sh

####################################
# INSTALLATION PREPARATION - QUERY #
####################################
# You will be asked several questions to determine what gets installed.
fg_blue ; print_centered "=" "=" ; reset_colour
print_centered "INSTALLATION PREPERATION - QUERY"
fg_blue ; print_centered "=" "=" ; reset_colour
echo

# Kernel
# You will be presented with 5 kernel choices and prompted to pick one.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Linux
echo "Which kernel would you like to install?"
echo -e "${BOLD}${FG_BLUE}1) Linux (Default)${END}"
echo "2) Linux-Hardened"
echo "3) Linux-LTS"
echo "4) Linux-RT"
echo "5) Linux-Zen"
echo -ne "$ARROW";read -r KERNEL
echo

# Turn your choice into a variable so that it can be used during the installation stage.
case $KERNEL in

    1 )

        KERNEL="linux"
        KERNEL_HEADERS="linux-headers"

    ;;

    2 )

        KERNEL="linux-hardened"
        KERNEL_HEADERS="linux-hardened-headers"

    ;;

    3 )

        KERNEL="linux-lts"
        KERNEL_HEADERS="linux-lts-headers"

    ;;

    4 )

        KERNEL="linux-rt"
        KERNEL_HEADERS="linux-rt-headers"

    ;;

    5 )

        KERNEL="linux-zen"
        KERNEL_HEADERS="linux-zen-headers"

    ;;

    * )

        KERNEL="linux"
        KERNEL_HEADERS="linux-headers"

    ;;

esac

# Development Toolkit
# You will be asked if you want your development toolkit installed on the new system.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install your development toolkit on the new system?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r DEVELOPMENT_TOOLKIT
echo

case $DEVELOPMENT_TOOLKIT in

    Y | y )

    ;;

    N | n )

        echo -e "${FG_L_RED}You chose not to install your development toolkit.${END}"
        echo "Neovim will be installed on the system as you will need at least a text editor for troubleshooting."
        echo
    
    ;;

    * )

    ;;

esac

# Video Drivers
# You will be presented with 5 video driver choices and prompted to pick one.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Open-Source Drivers
echo "Which video drivers would you like to install?"
echo -e "${BOLD}${FG_BLUE}1) All open-source drivers (Default)${END}"
echo "2) AMD"
echo "3) Intel"
echo "4) nVidia"
echo "5) Virtualbox / VMWare"
echo -ne "$ARROW";read -r VIDEO
echo

# If you choose nVidia, you will be presented with 3 video driver choices and prompted to pick one.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Open Kernel Modules
case $VIDEO in

    1 )

    ;;

    2 )

    ;;

    3 )

    ;;

    4 )

        echo "Which nVidia drivers would you like to install?"
        echo -e "${BOLD}${FG_BLUE}1) Open Kernel Modules (Default)${END}"
        echo "2) Open-Source Nouveau Drivers"
        echo "3) Proprietary"
        echo -ne "$ARROW";read -r NVIDIA
        echo

    ;;

    5 )

    ;;

    * )

    ;;

esac

# Audio Server
# You will be presented with 2 audio server choices and prompted to pick one.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Pipewire
echo "Which audio server would you like to install?"
echo -e "${BOLD}${FG_BLUE}1) Pipewire (Default)${END}"
echo "2) PulseAudio"
echo -ne "$ARROW";read -r AUDIO
echo

# Printer Support
# You will be asked if you want printer support on the new system.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want printer support on the new system?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r PRINTERS
echo

# Virtualization Toolkit
# You will be asked if you want your development toolkit installed on the new system.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : Yes
echo "Do you want to install your virtualization toolkit on the new system?"
echo -e "${BOLD}${FG_BLUE}[Y]es (Default)${END}"
echo "[N]o"
echo -ne "$ARROW";read -r VIRTUALIZATION_TOOLKIT
echo

# Desktop Environment
# You will be presented with 3 desktop environment choices and prompted to pick one.
# If your input is blank or does not match one of the choices, the default option will be invoked.
# Default option : KDE Plasma
echo "Which desktop environment would you like to install?" 
echo -e "${BOLD}${FG_BLUE}1) KDE Plasma (Default)${END}"
echo "2) Gnome"
echo "3) XFCE"
echo -ne "$ARROW";read -r DESKTOP
echo

# Desktop Applications
# Launch a seperate script that will determine which desktop applications you would like to install.
source /root/arnix/pacstrap/desktop-applications-query.sh

# Bootloader
# You will be present with two bootloader choices and prompted to pick one.
# If your input is blank, the default option will be invoked.
# Default option : rEFInd
echo "Which bootloader would you like to install?" 
echo -e "${BOLD}${FG_BLUE}1) GRUB (Default)${END}"
echo "2) Systemd-Boot"
echo "3) rEFInd"
echo -ne "$ARROW";read -r BOOTLOADER
echo

# Export Variables
export KERNEL
export KERNEL_HEADERS
export DEVELOPMENT_TOOLKIT
export VIDEO
export NVIDIA
export AUDIO
export PRINTERS
export VIRTUALIZATION_TOOLKIT
export DESKTOP
export BOOTLOADER
